<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $titile; ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo site_url(); ?>assets/admin_template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo site_url(); ?>assets/admin_template/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<?php echo site_url(); ?>assets/admin_template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-user"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo $this->session->userdata('level'); ?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <?php if($this->session->userdata('level') != 'user'){ ?>
      <li class="nav-item <?php echo $head_menu=='Dashboard'?'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url(); ?>sa">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
      </li>
      <?php }else{ ?>
      <li class="nav-item <?php echo $head_menu=='Kuisioner'?'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url(); ?>u">
        <i class="fas fa-fw fa-book"></i>
        <span>Kuisioner</span></a>
      </li>
      <li class="nav-item <?php echo $head_menu=='Hasil Kuisioner'?'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url(); ?>u/hk">
        <i class="fas fa-fw fa-book"></i>
        <span>Hasil Kuisioner</span></a>
      </li>
      <?php } ?>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <?php if($this->session->userdata('level') != 'user'){ ?>

      <!-- Heading -->
      <div class="sidebar-heading">
        Master
      </div>

      <!-- Nav Item - User -->
      <li class="nav-item <?php echo $head_menu=='Master User'?'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url(); ?>sa/mu">
          <i class="fas fa-fw fa-user"></i>
          <span>User</span></a>
      </li>

      <!-- Nav Item - Kuisioner -->
      <li class="nav-item <?php echo $head_menu=='Master Kuisioner'?'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url(); ?>sa/mk">
          <i class="fas fa-fw fa-book"></i>
          <span>Kuisioner</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Result
      </div>

      <!-- Nav Item - Kuisioner -->
      <li class="nav-item <?php echo $head_menu=='Hasil Kuisioner'?'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url(); ?>sa/hk">
          <i class="fas fa-fw fa-book"></i>
          <span>Kuisioner</span></a>
      </li>
      
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <?php } ?>
      
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            
            <?php $message = $this->session->flashdata('success');
            if($message){
                echo '<div class="text-center alert alert-success">' .$message. '</div>';
            } ?>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $this->session->userdata('nama_user'); ?></span>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        
                <?php if($this->session->userdata('level') != 'user'){ ?>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>sa/p/<?php echo $this->session->userdata('id_user'); ?>">
                <?php }else{ ?>
                  <a class="dropdown-item" href="<?php echo site_url(); ?>u/p/<?php echo $this->session->userdata('id_user'); ?>">
                <?php } ?>

                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->