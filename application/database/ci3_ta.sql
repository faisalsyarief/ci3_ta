-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2020 at 07:08 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci3_ta`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_hasil_kuisioner`
--

CREATE TABLE `t_hasil_kuisioner` (
  `id_hasil_kuisioner` varchar(10) NOT NULL,
  `id_user` varchar(10) NOT NULL,
  `pertanyaan` text NOT NULL,
  `jawaban` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `tgl_pengisian` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_hasil_kuisioner`
--

INSERT INTO `t_hasil_kuisioner` (`id_hasil_kuisioner`, `id_user`, `pertanyaan`, `jawaban`, `file`, `tgl_pengisian`) VALUES
('HK00000001', 'USR0000003', '[\"Reported By (Roving QC)\",\"Inspection Date\",\"Inspection Time\",\"Cell Production\",\"Manager Cell\",\"SOP & OIB Tersedia dan sesuai di setiap line\",\"Apakah cutting dies dan cutting board dalam keadaan baik\",\"Periksa jumlah cutting layer sesuai OIB\",\"Periksa Operator menggunakan JIG seusia OIB\",\"Lubang eyestay terpotong sempurna\",\"Marking \\/ Printing terlihat jelas\",\"Painting O\\/S konsisten mengikuti pattern dan margin\",\"Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai\",\"Apakah benang dan jarum mengikuti spec sesuai dengan OIB\",\"Periksa semua CTQ memiliki confirmation sample yg telah disetujui\",\"Periksa dan catat sensitivitas harian metal detector\",\"Temperatur dan TImer BPM sesuai dengan OIB\",\"Blowing time Toe Steaming \\/ Steam Upper sesuai dengan OIB\",\"Toe Mold jig \\/mold dan pengaturan yang digunakan sesuai dengan OIB\",\"Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui\",\"Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk\",\"Lasting, laser line  \\\"merah\\\" berada di centerline stroble pada +\\/- 1mm\",\"Lasting, pastikan posisi hole pin berada diujung laste +\\/- 1mm\",\"Toe marking, pastikan garis gauge sesuai standar\",\"Gauge marking, cek marking N logo sesuai standar\",\"Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan\",\"Cementing, Priming \\/ cementing diaplikasikan merata dan menutupi marking line max +\\/- 2mm\",\"Vamp Shape mengikuti bentuk laste bagian depan & belakang\",\"Toe Spring\",\"Heel Height\",\"Collar Opening\",\"Eyestay Spacing\"]', '[\"Roving QC 1\",\"2020-04-18\",\"Morning\",\"1\",\"Manager Cell 1\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\"]', 'main-qimg-6a7f84de804a50806a84ec88e927a0716.jpg', '2020-04-18'),
('HK00000002', 'USR0000003', '[\"Reported By (Roving QC)\",\"Inspection Date\",\"Inspection Time\",\"Cell Production\",\"Manager Cell\",\"SOP & OIB Tersedia dan sesuai di setiap line\",\"Apakah cutting dies dan cutting board dalam keadaan baik\",\"Periksa jumlah cutting layer sesuai OIB\",\"Periksa Operator menggunakan JIG seusia OIB\",\"Lubang eyestay terpotong sempurna\",\"Marking \\/ Printing terlihat jelas\",\"Painting O\\/S konsisten mengikuti pattern dan margin\",\"Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai\",\"Apakah benang dan jarum mengikuti spec sesuai dengan OIB\",\"Periksa semua CTQ memiliki confirmation sample yg telah disetujui\",\"Periksa dan catat sensitivitas harian metal detector\",\"Temperatur dan TImer BPM sesuai dengan OIB\",\"Blowing time Toe Steaming \\/ Steam Upper sesuai dengan OIB\",\"Toe Mold jig \\/mold dan pengaturan yang digunakan sesuai dengan OIB\",\"Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui\",\"Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk\",\"Lasting, laser line  \\\"merah\\\" berada di centerline stroble pada +\\/- 1mm\",\"Lasting, pastikan posisi hole pin berada diujung laste +\\/- 1mm\",\"Toe marking, pastikan garis gauge sesuai standar\",\"Gauge marking, cek marking N logo sesuai standar\",\"Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan\",\"Cementing, Priming \\/ cementing diaplikasikan merata dan menutupi marking line max +\\/- 2mm\",\"Vamp Shape mengikuti bentuk laste bagian depan & belakang\",\"Toe Spring\",\"Heel Height\",\"Collar Opening\",\"Eyestay Spacing\"]', '[\"Roving QC 2\",\"2020-04-18\",\"Afternoon\",\"2\",\"Manager Cell\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\"]', 'bg-01.jpg', '2020-04-18'),
('HK00000003', 'USR0000004', '[\"Reported By (Roving QC)\",\"Inspection Date\",\"Inspection Time\",\"Cell Production\",\"Manager Cell\",\"SOP & OIB Tersedia dan sesuai di setiap line\",\"Apakah cutting dies dan cutting board dalam keadaan baik\",\"Periksa jumlah cutting layer sesuai OIB\",\"Periksa Operator menggunakan JIG seusia OIB\",\"Lubang eyestay terpotong sempurna\",\"Marking \\/ Printing terlihat jelas\",\"Painting O\\/S konsisten mengikuti pattern dan margin\",\"Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai\",\"Apakah benang dan jarum mengikuti spec sesuai dengan OIB\",\"Periksa semua CTQ memiliki confirmation sample yg telah disetujui\",\"Periksa dan catat sensitivitas harian metal detector\",\"Temperatur dan TImer BPM sesuai dengan OIB\",\"Blowing time Toe Steaming \\/ Steam Upper sesuai dengan OIB\",\"Toe Mold jig \\/mold dan pengaturan yang digunakan sesuai dengan OIB\",\"Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui\",\"Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk\",\"Lasting, laser line  \\\"merah\\\" berada di centerline stroble pada +\\/- 1mm\",\"Lasting, pastikan posisi hole pin berada diujung laste +\\/- 1mm\",\"Toe marking, pastikan garis gauge sesuai standar\",\"Gauge marking, cek marking N logo sesuai standar\",\"Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan\",\"Cementing, Priming \\/ cementing diaplikasikan merata dan menutupi marking line max +\\/- 2mm\",\"Vamp Shape mengikuti bentuk laste bagian depan & belakang\",\"Toe Spring\",\"Heel Height\",\"Collar Opening\",\"Eyestay Spacing\"]', '[\"Roving QC 1\",\"2020-04-17\",\"Morning\",\"1\",\"Manager Cell 1\",\"Ya\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\",\"Ya\",\"Tidak\"]', 'bg_login.jpeg', '2020-04-18'),
('HK00000004', 'USR0000003', '[\"Reported By (Roving QC)\",\"Inspection Date\",\"Inspection Time\",\"Cell Production\",\"Manager Cell\",\"SOP & OIB Tersedia dan sesuai di setiap line\",\"Apakah cutting dies dan cutting board dalam keadaan baik\",\"Periksa jumlah cutting layer sesuai OIB\",\"Periksa Operator menggunakan JIG seusia OIB\",\"Lubang eyestay terpotong sempurna\",\"Marking \\/ Printing terlihat jelas\",\"Painting O\\/S konsisten mengikuti pattern dan margin\",\"Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai\",\"Apakah benang dan jarum mengikuti spec sesuai dengan OIB\",\"Periksa semua CTQ memiliki confirmation sample yg telah disetujui\",\"Periksa dan catat sensitivitas harian metal detector\",\"Temperatur dan TImer BPM sesuai dengan OIB\",\"Blowing time Toe Steaming \\/ Steam Upper sesuai dengan OIB\",\"Toe Mold jig \\/mold dan pengaturan yang digunakan sesuai dengan OIB\",\"Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui\",\"Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk\",\"Lasting, laser line  \\\"merah\\\" berada di centerline stroble pada +\\/- 1mm\",\"Lasting, pastikan posisi hole pin berada diujung laste +\\/- 1mm\",\"Toe marking, pastikan garis gauge sesuai standar\",\"Gauge marking, cek marking N logo sesuai standar\",\"Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan\",\"Cementing, Priming \\/ cementing diaplikasikan merata dan menutupi marking line max +\\/- 2mm\",\"Vamp Shape mengikuti bentuk laste bagian depan & belakang\",\"Toe Spring\",\"Heel Height\",\"Collar Opening\",\"Eyestay Spacing\"]', '[\"Roving QC 1\",\"2020-04-17\",\"Morning\",\"1\",\"Manager Cell 1\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\"]', 'main-qimg-6a7f84de804a50806a84ec88e927a0717.jpg', '2020-04-18'),
('HK00000005', 'USR0000004', '[\"Reported By (Roving QC)\",\"Inspection Date\",\"Inspection Time\",\"Cell Production\",\"Manager Cell\",\"SOP & OIB Tersedia dan sesuai di setiap line\",\"Apakah cutting dies dan cutting board dalam keadaan baik\",\"Periksa jumlah cutting layer sesuai OIB\",\"Periksa Operator menggunakan JIG seusia OIB\",\"Lubang eyestay terpotong sempurna\",\"Marking \\/ Printing terlihat jelas\",\"Painting O\\/S konsisten mengikuti pattern dan margin\",\"Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai\",\"Apakah benang dan jarum mengikuti spec sesuai dengan OIB\",\"Periksa semua CTQ memiliki confirmation sample yg telah disetujui\",\"Periksa dan catat sensitivitas harian metal detector\",\"Temperatur dan TImer BPM sesuai dengan OIB\",\"Blowing time Toe Steaming \\/ Steam Upper sesuai dengan OIB\",\"Toe Mold jig \\/mold dan pengaturan yang digunakan sesuai dengan OIB\",\"Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui\",\"Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk\",\"Lasting, laser line  \\\"merah\\\" berada di centerline stroble pada +\\/- 1mm\",\"Lasting, pastikan posisi hole pin berada diujung laste +\\/- 1mm\",\"Toe marking, pastikan garis gauge sesuai standar\",\"Gauge marking, cek marking N logo sesuai standar\",\"Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan\",\"Cementing, Priming \\/ cementing diaplikasikan merata dan menutupi marking line max +\\/- 2mm\",\"Vamp Shape mengikuti bentuk laste bagian depan & belakang\",\"Toe Spring\",\"Heel Height\",\"Collar Opening\",\"Eyestay Spacing\"]', '[\"Roving QC\",\"2020-04-17\",\"Morning\",\"1\",\"Manager Cell\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\",\"Ya\"]', 'main-qimg-6a7f84de804a50806a84ec88e927a0718.jpg', '2020-04-17');

-- --------------------------------------------------------

--
-- Table structure for table `t_hk_graph`
--

CREATE TABLE `t_hk_graph` (
  `id_graph` int(11) NOT NULL,
  `pertanyaan` varchar(255) NOT NULL,
  `jenis_pertanyaan` varchar(255) NOT NULL,
  `jawaban` varchar(255) NOT NULL,
  `tgl_pengisian` date NOT NULL,
  `id_user` varchar(10) NOT NULL,
  `cell_production` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_hk_graph`
--

INSERT INTO `t_hk_graph` (`id_graph`, `pertanyaan`, `jenis_pertanyaan`, `jawaban`, `tgl_pengisian`, `id_user`, `cell_production`) VALUES
(203, 'Reported By (Roving QC)', '', 'Roving QC 1', '2020-04-18', 'USR0000003', 1),
(204, 'Inspection Date', '', '2020-04-18', '2020-04-18', 'USR0000003', 1),
(205, 'Inspection Time', '', 'Morning', '2020-04-18', 'USR0000003', 1),
(206, 'Cell Production', '', '1', '2020-04-18', 'USR0000003', 1),
(207, 'Manager Cell', '', 'Manager Cell 1', '2020-04-18', 'USR0000003', 1),
(208, 'SOP & OIB Tersedia dan sesuai di setiap line', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000003', 1),
(209, 'Apakah cutting dies dan cutting board dalam keadaan baik', 'Proses Cutting', 'Tidak', '2020-04-18', 'USR0000003', 1),
(210, 'Periksa jumlah cutting layer sesuai OIB', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000003', 1),
(211, 'Periksa Operator menggunakan JIG seusia OIB', 'Proses NO-Sew', 'Tidak', '2020-04-18', 'USR0000003', 1),
(212, 'Lubang eyestay terpotong sempurna', 'Proses Punching', 'Ya', '2020-04-18', 'USR0000003', 1),
(213, 'Marking / Printing terlihat jelas', 'Proses Printing', 'Tidak', '2020-04-18', 'USR0000003', 1),
(214, 'Painting O/S konsisten mengikuti pattern dan margin', 'Proses Painting', 'Ya', '2020-04-18', 'USR0000003', 1),
(215, 'Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai', 'Proses Stitching', 'Tidak', '2020-04-18', 'USR0000003', 1),
(216, 'Apakah benang dan jarum mengikuti spec sesuai dengan OIB', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000003', 1),
(217, 'Periksa semua CTQ memiliki confirmation sample yg telah disetujui', 'Proses Stitching', 'Tidak', '2020-04-18', 'USR0000003', 1),
(218, 'Periksa dan catat sensitivitas harian metal detector', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(219, 'Temperatur dan TImer BPM sesuai dengan OIB', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 1),
(220, 'Blowing time Toe Steaming / Steam Upper sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(221, 'Toe Mold jig /mold dan pengaturan yang digunakan sesuai dengan OIB', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 1),
(222, 'Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(223, 'Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 1),
(224, 'Lasting, laser line  \"merah\" berada di centerline stroble pada +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(225, 'Lasting, pastikan posisi hole pin berada diujung laste +/- 1mm', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 1),
(226, 'Toe marking, pastikan garis gauge sesuai standar', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(227, 'Gauge marking, cek marking N logo sesuai standar', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 1),
(228, 'Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(229, 'Cementing, Priming / cementing diaplikasikan merata dan menutupi marking line max +/- 2mm', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 1),
(230, 'Vamp Shape mengikuti bentuk laste bagian depan & belakang', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(231, 'Toe Spring', 'CCQP', 'Tidak', '2020-04-18', 'USR0000003', 1),
(232, 'Heel Height', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(233, 'Collar Opening', 'CCQP', 'Tidak', '2020-04-18', 'USR0000003', 1),
(234, 'Eyestay Spacing', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(235, 'Reported By (Roving QC)', '', 'Roving QC 2', '2020-04-18', 'USR0000003', 2),
(236, 'Inspection Date', '', '2020-04-18', '2020-04-18', 'USR0000003', 2),
(237, 'Inspection Time', '', 'Afternoon', '2020-04-18', 'USR0000003', 2),
(238, 'Cell Production', '', '2', '2020-04-18', 'USR0000003', 2),
(239, 'Manager Cell', '', 'Manager Cell', '2020-04-18', 'USR0000003', 2),
(240, 'SOP & OIB Tersedia dan sesuai di setiap line', 'Proses Cutting', 'Tidak', '2020-04-18', 'USR0000003', 2),
(241, 'Apakah cutting dies dan cutting board dalam keadaan baik', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000003', 2),
(242, 'Periksa jumlah cutting layer sesuai OIB', 'Proses Cutting', 'Tidak', '2020-04-18', 'USR0000003', 2),
(243, 'Periksa Operator menggunakan JIG seusia OIB', 'Proses NO-Sew', 'Ya', '2020-04-18', 'USR0000003', 2),
(244, 'Lubang eyestay terpotong sempurna', 'Proses Punching', 'Tidak', '2020-04-18', 'USR0000003', 2),
(245, 'Marking / Printing terlihat jelas', 'Proses Printing', 'Ya', '2020-04-18', 'USR0000003', 2),
(246, 'Painting O/S konsisten mengikuti pattern dan margin', 'Proses Painting', 'Tidak', '2020-04-18', 'USR0000003', 2),
(247, 'Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000003', 2),
(248, 'Apakah benang dan jarum mengikuti spec sesuai dengan OIB', 'Proses Stitching', 'Tidak', '2020-04-18', 'USR0000003', 2),
(249, 'Periksa semua CTQ memiliki confirmation sample yg telah disetujui', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000003', 2),
(250, 'Periksa dan catat sensitivitas harian metal detector', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 2),
(251, 'Temperatur dan TImer BPM sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 2),
(252, 'Blowing time Toe Steaming / Steam Upper sesuai dengan OIB', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 2),
(253, 'Toe Mold jig /mold dan pengaturan yang digunakan sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 2),
(254, 'Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 2),
(255, 'Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 2),
(256, 'Lasting, laser line  \"merah\" berada di centerline stroble pada +/- 1mm', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 2),
(257, 'Lasting, pastikan posisi hole pin berada diujung laste +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 2),
(258, 'Toe marking, pastikan garis gauge sesuai standar', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 2),
(259, 'Gauge marking, cek marking N logo sesuai standar', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 2),
(260, 'Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000003', 2),
(261, 'Cementing, Priming / cementing diaplikasikan merata dan menutupi marking line max +/- 2mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 2),
(262, 'Vamp Shape mengikuti bentuk laste bagian depan & belakang', 'CCQP', 'Tidak', '2020-04-18', 'USR0000003', 2),
(263, 'Toe Spring', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 2),
(264, 'Heel Height', 'CCQP', 'Tidak', '2020-04-18', 'USR0000003', 2),
(265, 'Collar Opening', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 2),
(266, 'Eyestay Spacing', 'CCQP', 'Tidak', '2020-04-18', 'USR0000003', 2),
(267, 'Reported By (Roving QC)', '', 'Roving QC 1', '2020-04-18', 'USR0000004', 1),
(268, 'Inspection Date', '', '2020-04-17', '2020-04-18', 'USR0000004', 1),
(269, 'Inspection Time', '', 'Morning', '2020-04-18', 'USR0000004', 1),
(270, 'Cell Production', '', '1', '2020-04-18', 'USR0000004', 1),
(271, 'Manager Cell', '', 'Manager Cell 1', '2020-04-18', 'USR0000004', 1),
(272, 'SOP & OIB Tersedia dan sesuai di setiap line', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000004', 1),
(273, 'Apakah cutting dies dan cutting board dalam keadaan baik', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000004', 1),
(274, 'Periksa jumlah cutting layer sesuai OIB', 'Proses Cutting', 'Tidak', '2020-04-18', 'USR0000004', 1),
(275, 'Periksa Operator menggunakan JIG seusia OIB', 'Proses NO-Sew', 'Ya', '2020-04-18', 'USR0000004', 1),
(276, 'Lubang eyestay terpotong sempurna', 'Proses Punching', 'Tidak', '2020-04-18', 'USR0000004', 1),
(277, 'Marking / Printing terlihat jelas', 'Proses Printing', 'Ya', '2020-04-18', 'USR0000004', 1),
(278, 'Painting O/S konsisten mengikuti pattern dan margin', 'Proses Painting', 'Tidak', '2020-04-18', 'USR0000004', 1),
(279, 'Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000004', 1),
(280, 'Apakah benang dan jarum mengikuti spec sesuai dengan OIB', 'Proses Stitching', 'Tidak', '2020-04-18', 'USR0000004', 1),
(281, 'Periksa semua CTQ memiliki confirmation sample yg telah disetujui', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000004', 1),
(282, 'Periksa dan catat sensitivitas harian metal detector', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000004', 1),
(283, 'Temperatur dan TImer BPM sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000004', 1),
(284, 'Blowing time Toe Steaming / Steam Upper sesuai dengan OIB', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000004', 1),
(285, 'Toe Mold jig /mold dan pengaturan yang digunakan sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000004', 1),
(286, 'Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000004', 1),
(287, 'Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000004', 1),
(288, 'Lasting, laser line  \"merah\" berada di centerline stroble pada +/- 1mm', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000004', 1),
(289, 'Lasting, pastikan posisi hole pin berada diujung laste +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000004', 1),
(290, 'Toe marking, pastikan garis gauge sesuai standar', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000004', 1),
(291, 'Gauge marking, cek marking N logo sesuai standar', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000004', 1),
(292, 'Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan', 'Proses Assembly', 'Tidak', '2020-04-18', 'USR0000004', 1),
(293, 'Cementing, Priming / cementing diaplikasikan merata dan menutupi marking line max +/- 2mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000004', 1),
(294, 'Vamp Shape mengikuti bentuk laste bagian depan & belakang', 'CCQP', 'Tidak', '2020-04-18', 'USR0000004', 1),
(295, 'Toe Spring', 'CCQP', 'Ya', '2020-04-18', 'USR0000004', 1),
(296, 'Heel Height', 'CCQP', 'Tidak', '2020-04-18', 'USR0000004', 1),
(297, 'Collar Opening', 'CCQP', 'Ya', '2020-04-18', 'USR0000004', 1),
(298, 'Eyestay Spacing', 'CCQP', 'Tidak', '2020-04-18', 'USR0000004', 1),
(299, 'Reported By (Roving QC)', '', 'Roving QC 1', '2020-04-18', 'USR0000003', 1),
(300, 'Inspection Date', '', '2020-04-17', '2020-04-18', 'USR0000003', 1),
(301, 'Inspection Time', '', 'Morning', '2020-04-18', 'USR0000003', 1),
(302, 'Cell Production', '', '1', '2020-04-18', 'USR0000003', 1),
(303, 'Manager Cell', '', 'Manager Cell 1', '2020-04-18', 'USR0000003', 1),
(304, 'SOP & OIB Tersedia dan sesuai di setiap line', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000003', 1),
(305, 'Apakah cutting dies dan cutting board dalam keadaan baik', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000003', 1),
(306, 'Periksa jumlah cutting layer sesuai OIB', 'Proses Cutting', 'Ya', '2020-04-18', 'USR0000003', 1),
(307, 'Periksa Operator menggunakan JIG seusia OIB', 'Proses NO-Sew', 'Ya', '2020-04-18', 'USR0000003', 1),
(308, 'Lubang eyestay terpotong sempurna', 'Proses Punching', 'Ya', '2020-04-18', 'USR0000003', 1),
(309, 'Marking / Printing terlihat jelas', 'Proses Printing', 'Ya', '2020-04-18', 'USR0000003', 1),
(310, 'Painting O/S konsisten mengikuti pattern dan margin', 'Proses Painting', 'Ya', '2020-04-18', 'USR0000003', 1),
(311, 'Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000003', 1),
(312, 'Apakah benang dan jarum mengikuti spec sesuai dengan OIB', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000003', 1),
(313, 'Periksa semua CTQ memiliki confirmation sample yg telah disetujui', 'Proses Stitching', 'Ya', '2020-04-18', 'USR0000003', 1),
(314, 'Periksa dan catat sensitivitas harian metal detector', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(315, 'Temperatur dan TImer BPM sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(316, 'Blowing time Toe Steaming / Steam Upper sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(317, 'Toe Mold jig /mold dan pengaturan yang digunakan sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(318, 'Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(319, 'Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(320, 'Lasting, laser line  \"merah\" berada di centerline stroble pada +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(321, 'Lasting, pastikan posisi hole pin berada diujung laste +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(322, 'Toe marking, pastikan garis gauge sesuai standar', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(323, 'Gauge marking, cek marking N logo sesuai standar', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(324, 'Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(325, 'Cementing, Priming / cementing diaplikasikan merata dan menutupi marking line max +/- 2mm', 'Proses Assembly', 'Ya', '2020-04-18', 'USR0000003', 1),
(326, 'Vamp Shape mengikuti bentuk laste bagian depan & belakang', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(327, 'Toe Spring', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(328, 'Heel Height', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(329, 'Collar Opening', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(330, 'Eyestay Spacing', 'CCQP', 'Ya', '2020-04-18', 'USR0000003', 1),
(331, 'Reported By (Roving QC)', '', 'Roving QC', '2020-04-17', 'USR0000004', 1),
(332, 'Inspection Date', '', '2020-04-17', '2020-04-17', 'USR0000004', 1),
(333, 'Inspection Time', '', 'Morning', '2020-04-17', 'USR0000004', 1),
(334, 'Cell Production', '', '1', '2020-04-17', 'USR0000004', 1),
(335, 'Manager Cell', '', 'Manager Cell', '2020-04-17', 'USR0000004', 1),
(336, 'SOP & OIB Tersedia dan sesuai di setiap line', 'Proses Cutting', 'Ya', '2020-04-17', 'USR0000004', 1),
(337, 'Apakah cutting dies dan cutting board dalam keadaan baik', 'Proses Cutting', 'Ya', '2020-04-17', 'USR0000004', 1),
(338, 'Periksa jumlah cutting layer sesuai OIB', 'Proses Cutting', 'Ya', '2020-04-17', 'USR0000004', 1),
(339, 'Periksa Operator menggunakan JIG seusia OIB', 'Proses NO-Sew', 'Ya', '2020-04-17', 'USR0000004', 1),
(340, 'Lubang eyestay terpotong sempurna', 'Proses Punching', 'Ya', '2020-04-17', 'USR0000004', 1),
(341, 'Marking / Printing terlihat jelas', 'Proses Printing', 'Ya', '2020-04-17', 'USR0000004', 1),
(342, 'Painting O/S konsisten mengikuti pattern dan margin', 'Proses Painting', 'Ya', '2020-04-17', 'USR0000004', 1),
(343, 'Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai', 'Proses Stitching', 'Ya', '2020-04-17', 'USR0000004', 1),
(344, 'Apakah benang dan jarum mengikuti spec sesuai dengan OIB', 'Proses Stitching', 'Ya', '2020-04-17', 'USR0000004', 1),
(345, 'Periksa semua CTQ memiliki confirmation sample yg telah disetujui', 'Proses Stitching', 'Ya', '2020-04-17', 'USR0000004', 1),
(346, 'Periksa dan catat sensitivitas harian metal detector', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(347, 'Temperatur dan TImer BPM sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(348, 'Blowing time Toe Steaming / Steam Upper sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(349, 'Toe Mold jig /mold dan pengaturan yang digunakan sesuai dengan OIB', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(350, 'Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(351, 'Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(352, 'Lasting, laser line  \"merah\" berada di centerline stroble pada +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(353, 'Lasting, pastikan posisi hole pin berada diujung laste +/- 1mm', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(354, 'Toe marking, pastikan garis gauge sesuai standar', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(355, 'Gauge marking, cek marking N logo sesuai standar', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(356, 'Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(357, 'Cementing, Priming / cementing diaplikasikan merata dan menutupi marking line max +/- 2mm', 'Proses Assembly', 'Ya', '2020-04-17', 'USR0000004', 1),
(358, 'Vamp Shape mengikuti bentuk laste bagian depan & belakang', 'CCQP', 'Ya', '2020-04-17', 'USR0000004', 1),
(359, 'Toe Spring', 'CCQP', 'Ya', '2020-04-17', 'USR0000004', 1),
(360, 'Heel Height', 'CCQP', 'Ya', '2020-04-17', 'USR0000004', 1),
(361, 'Collar Opening', 'CCQP', 'Ya', '2020-04-17', 'USR0000004', 1),
(362, 'Eyestay Spacing', 'CCQP', 'Ya', '2020-04-17', 'USR0000004', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_kuisioner`
--

CREATE TABLE `t_kuisioner` (
  `id_kuisioner` varchar(10) NOT NULL,
  `pertanyaan` text NOT NULL,
  `jenis_pertanyaan` varchar(255) NOT NULL,
  `tipe_pertanyaan` varchar(10) NOT NULL,
  `value_tipe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_kuisioner`
--

INSERT INTO `t_kuisioner` (`id_kuisioner`, `pertanyaan`, `jenis_pertanyaan`, `tipe_pertanyaan`, `value_tipe`) VALUES
('QUIZ000001', 'Reported By (Roving QC)', '', 'text', ''),
('QUIZ000002', 'Inspection Date', '', 'date', ''),
('QUIZ000003', 'Inspection Time', '', 'radio', 'Morning;Afternoon'),
('QUIZ000004', 'Cell Production', '', 'number', ''),
('QUIZ000005', 'Manager Cell', '', 'text', ''),
('QUIZ000006', 'SOP &amp; OIB Tersedia dan sesuai di setiap line', 'Proses Cutting', 'radio', 'Ya;Tidak'),
('QUIZ000007', 'Apakah cutting dies dan cutting board dalam keadaan baik', 'Proses Cutting', 'radio', 'Ya;Tidak'),
('QUIZ000008', 'Periksa jumlah cutting layer sesuai OIB', 'Proses Cutting', 'radio', 'Ya;Tidak'),
('QUIZ000009', 'Periksa Operator menggunakan JIG seusia OIB', 'Proses NO-Sew', 'radio', 'Ya;Tidak'),
('QUIZ000010', 'Lubang eyestay terpotong sempurna', 'Proses Punching', 'radio', 'Ya;Tidak'),
('QUIZ000011', 'Marking / Printing terlihat jelas', 'Proses Printing', 'radio', 'Ya;Tidak'),
('QUIZ000012', 'Painting O/S konsisten mengikuti pattern dan margin', 'Proses Painting', 'radio', 'Ya;Tidak'),
('QUIZ000013', 'Apakah penanganan patahan jarum sesuai dengan prosedur yang sesuai', 'Proses Stitching', 'radio', 'Ya;Tidak'),
('QUIZ000014', 'Apakah benang dan jarum mengikuti spec sesuai dengan OIB', 'Proses Stitching', 'radio', 'Ya;Tidak'),
('QUIZ000015', 'Periksa semua CTQ memiliki confirmation sample yg telah disetujui', 'Proses Stitching', 'radio', 'Ya;Tidak'),
('QUIZ000016', 'Periksa dan catat sensitivitas harian metal detector', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000017', 'Temperatur dan TImer BPM sesuai dengan OIB', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000018', 'Blowing time Toe Steaming / Steam Upper sesuai dengan OIB', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000019', 'Toe Mold jig /mold dan pengaturan yang digunakan sesuai dengan OIB', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000020', 'Cek semua CTQ dan sepatu memliki confirmation sample yang telah disetujui', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000021', 'Stroble stitching margin max 2mm, tidak longgar dan tidak ada jahitan yg menumpuk', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000022', 'Lasting, laser line  &quot;merah&quot; berada di centerline stroble pada +/- 1mm', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000023', 'Lasting, pastikan posisi hole pin berada diujung laste +/- 1mm', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000024', 'Toe marking, pastikan garis gauge sesuai standar', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000025', 'Gauge marking, cek marking N logo sesuai standar', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000026', 'Cementing, Sikat dan mangkok digantin sesuai jadwal yang ditentukan', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000027', 'Cementing, Priming / cementing diaplikasikan merata dan menutupi marking line max +/- 2mm', 'Proses Assembly', 'radio', 'Ya;Tidak'),
('QUIZ000028', 'Vamp Shape mengikuti bentuk laste bagian depan &amp; belakang', 'CCQP', 'radio', 'Ya;Tidak'),
('QUIZ000029', 'Toe Spring', 'CCQP', 'radio', 'Ya;Tidak'),
('QUIZ000030', 'Heel Height', 'CCQP', 'radio', 'Ya;Tidak'),
('QUIZ000031', 'Collar Opening', 'CCQP', 'radio', 'Ya;Tidak'),
('QUIZ000032', 'Eyestay Spacing', 'CCQP', 'radio', 'Ya;Tidak');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` varchar(10) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `level` varchar(10) NOT NULL,
  `send_email` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `nama_user`, `username`, `password`, `email`, `jk`, `level`, `send_email`) VALUES
('USR0000001', 'Sys Admin', 'sysadmin', 'sysadmin', 'sysadmin@gmail.com', 'Laki-Laki', 'sysadmin', 0),
('USR0000002', 'Admin', 'admin', 'admin', 'admin@gmail.com', 'Perempuan', 'admin', 0),
('USR0000003', 'User', 'user', 'user', 'user@gmail.com', 'Lain-Lain', 'user', 0),
('USR0000004', 'User 1', 'user1', 'user1', 'user1@gmail.com', 'Laki-Laki', 'user', 0),
('USR0000005', 'User 2', 'user2', 'user2', 'user2@gmail.com', 'Perempuan', 'user', 0),
('USR0000006', 'user3', 'user3', 'user3', 'user3@gmail.com', 'Lain-Lain', 'user', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_hk_graph`
--
ALTER TABLE `t_hk_graph`
  ADD PRIMARY KEY (`id_graph`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_hk_graph`
--
ALTER TABLE `t_hk_graph`
  MODIFY `id_graph` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
