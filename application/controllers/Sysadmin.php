<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sysadmin extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
		// SESSION UNTUK LOGIN
		if($this->session->userdata('level') == 'user'){
			$this->session->set_flashdata('error','Maaf, silahkan login sysadmin terlebih dahulu!');
			redirect('Login');
		}

		// LOAD MODEL
		$this->load->model('User_model');
		$this->load->model('Admin_model');
		
	}
	
	// DASHBOARD
	public function index() {
		$date_select="";
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index', $data);
		$this->load->view('page/footer', $data);
	}
	
	// DASHBOARD
	public function index_page1() {
		$date_select="";
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index1', $data);
		$this->load->view('page/footer', $data);
	}
	
	// DASHBOARD
	public function index_page2() {
		$date_select="";
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index2', $data);
		$this->load->view('page/footer', $data);
	}
	
	// DASHBOARD
	public function index_page3() {
		$date_select="";
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index3', $data);
		$this->load->view('page/footer', $data);
	}
	
	// DASHBOARD
	public function view_chart($view_chart) {
		$date_select="";
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
				'date_views'		=> $view_chart,
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> '',
				'date_views'		=> $view_chart,
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/view_chart', $data);
		$this->load->view('page/footer', $data);
	}

	// DASHBOARD BY DATE
	public function index_bydate($date_select) {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index', $data);
		$this->load->view('page/footer', $data,);
	}

	// DASHBOARD BY DATE
	public function index_bydate1($date_select) {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index1', $data);
		$this->load->view('page/footer', $data,);
	}

	// DASHBOARD BY DATE
	public function index_bydate2($date_select) {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/index2', $data);
		$this->load->view('page/footer', $data,);
	}

	// DASHBOARD BY DATE
	public function index_bydatev($date_views,$date_select) {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
				'date_views'		=> $date_views,
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Dashboard',
				'data_label_chart'	=> $this->Admin_model->get_data_label_chart($date_select),
				'data_picture'		=> $this->Admin_model->get_data_picture($date_select),
				'date_select'		=> $date_select,
				'date_views'		=> $date_views,
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/view_chart', $data);
		$this->load->view('page/footer', $data,);
	}
	
	// MASTER USER
	public function index_master_user() {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'	=> 'Panel Sysadmin',
				'head_menu'	=> 'Master User',
				'data_user'	=> $this->User_model->get_data_master_user(),
			);
		}else{
			$data = array(
				'titile'	=> 'Panel Admin',
				'head_menu'	=> 'Master User',
				'data_user'	=> $this->User_model->get_data_master_user(),
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/user/index', $data);
		$this->load->view('page/footer');
	}
	
	// MASTER ADD USER
	public function add_master_user() {
		$this->form_validation->set_rules('nama_user','Nama User','required');
		$this->form_validation->set_rules('username','Username','required|is_unique[t_user.username]');
		$this->form_validation->set_rules('password','Password','required|is_unique[t_user.password]');
		$this->form_validation->set_rules('email','E-Mail','required|valid_email|is_unique[t_user.email]');
		
		if($this->form_validation->run() == FALSE){
			if($this->session->userdata('level') == 'sysadmin'){
				$data = array(
					'titile'			=> 'Panel Sysadmin',
					'head_menu'			=> 'Master User',
					'head_menu_form'	=> 'Add User',
				);
			}else{
				$data = array(
					'titile'			=> 'Panel Admin',
					'head_menu'			=> 'Master User',
					'head_menu_form'	=> 'Add User',
				);
			}

			$this->load->view('page/header', $data);
			$this->load->view('sysadmin/user/add', $data);
			$this->load->view('page/footer');
		}else{
			$data_user = array (
				'id_user'		=> $this->User_model->get_user_id(),
				'nama_user'		=> set_value('nama_user'),
				'jk'			=> set_value('jk'),
				'email'			=> set_value('email'),
				'username'		=> set_value('username'),
				'password'		=> set_value('password'),
				'level'			=> set_value('level'),
				'send_email'	=> set_value('send_email'),
			);
			$this->User_model->insert_user($data_user);
			$this->session->set_flashdata('add','Anda berhasil menambahkan data user');
			redirect('sa/mu');
		}	
	}

	// MASTER UPDATE USER
	public function update_master_user($id_user) {
		$this->form_validation->set_rules('nama_user','Nama User','required');
		
		if($this->form_validation->run() == FALSE){
			if($this->session->userdata('level') == 'sysadmin'){
				$data = array(
					'titile'			=> 'Panel Sysadmin',
					'head_menu'			=> 'Master User',
					'head_menu_form'	=> 'Edit User',
					'user'				=> $this->User_model->find_user($id_user),
				);
			}else{
				$data = array(
					'titile'			=> 'Panel Admin',
					'head_menu'			=> 'Master User',
					'head_menu_form'	=> 'Edit User',
					'user'				=> $this->User_model->find_user($id_user),
				);
			}

			$this->load->view('page/header', $data);
			$this->load->view('sysadmin/user/update', $data);
			$this->load->view('page/footer');
		}else{
			$data_user = array (
				'nama_user'		=> set_value('nama_user'),
				'jk'			=> set_value('jk'),
				'email'			=> set_value('email'),
				'username'		=> set_value('username'),
				'password'		=> set_value('password'),
				'level'			=> set_value('level'),
				'send_email'	=> set_value('send_email'),
			);
			$this->User_model->update_user($id_user, $data_user);
			
			if(set_value('profile')){
				$this->session->set_flashdata('update','Anda berhasil meng-update profile');
				redirect('sa');
			}else{
				$this->session->set_flashdata('update','Anda berhasil meng-update data user');
				redirect('sa/mu');
			}
		}	
	}
	
	// MASTER DELETE USER
	public function delete_master_user($id_user) {
		$where = array('id_user' => $id_user);
		$this->User_model->delete($where,'t_user');
		$this->session->set_flashdata('delete','Anda berhasil menghapus data user');
		redirect('sa/mu');
	}
	
	// PROFILE
	public function index_profile($id_user) {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'	=> 'Panel Sysadmin',
				'head_menu'	=> 'Profile',
				'user'				=> $this->User_model->find_user($id_user),
			);
		}else{
			$data = array(
				'titile'	=> 'Panel Admin',
				'head_menu'	=> 'Profile',
				'user'				=> $this->User_model->find_user($id_user),
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/profile');
		$this->load->view('page/footer');
	}
	
	// MASTER KUISIONER
	public function index_master_kuisioner() {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'	=> 'Panel Sysadmin',
				'head_menu'	=> 'Master Kuisioner',
				'data_kuisioner'	=> $this->Admin_model->get_data_master_kuisioner(),
			);
		}else{
			$data = array(
				'titile'	=> 'Panel Admin',
				'head_menu'	=> 'Master Kuisioner',
				'data_kuisioner'	=> $this->Admin_model->get_data_master_kuisioner(),
			);
		}
        
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/kuisioner/index', $data);
		$this->load->view('page/footer');
	}
	
	// MASTER ADD KUISIONER
	public function add_master_kuisioner() {
		$this->form_validation->set_rules('pertanyaan','Pertanyaan','required');
		$this->form_validation->set_rules('tipe_pertanyaan','Tipe Pertanyaan','required');
		
		if($this->form_validation->run() == FALSE){
			if($this->session->userdata('level') == 'sysadmin'){
				$data = array(
					'titile'			=> 'Panel Sysadmin',
					'head_menu'			=> 'Master Kuisioner',
					'head_menu_form'	=> 'Add Kuisioner',
				);
			}else{
				$data = array(
					'titile'			=> 'Panel Admin',
					'head_menu'			=> 'Master Kuisioner',
					'head_menu_form'	=> 'Add Kuisioner',
				);
			}

			$this->load->view('page/header', $data);
			$this->load->view('sysadmin/kuisioner/add', $data);
			$this->load->view('page/footer');
		}else{
			if(set_value('tipe_pertanyaan') == 'radio'){
				$vt = set_value('value_tipe');
			}else{
				$vt = '';
			}
			$data_kuisioner = array (
				'id_kuisioner'		=> $this->Admin_model->get_kuisioner_id(),
				'pertanyaan'		=> set_value('pertanyaan'),
				'jenis_pertanyaan'	=> set_value('jenis_pertanyaan'),
				'tipe_pertanyaan'	=> set_value('tipe_pertanyaan'),
				'value_tipe'		=> $vt,
			);
			$this->Admin_model->insert_kuisioner($data_kuisioner);
			$this->session->set_flashdata('add','Anda berhasil menambahkan data kuisioner');
			redirect('sa/mk');
		}	
	}

	// MASTER UPDATE KUISIONER
	public function update_master_kuisioner($id_kuisioner) {
		$this->form_validation->set_rules('pertanyaan','Pertanyaan','required');
		$this->form_validation->set_rules('tipe_pertanyaan','Tipe Pertanyaan','required');
		
		if($this->form_validation->run() == FALSE){
			if($this->session->userdata('level') == 'sysadmin'){
				$data = array(
					'titile'			=> 'Panel Sysadmin',
					'head_menu'			=> 'Master Kuisioner',
					'head_menu_form'	=> 'Edit Kuisioner',
					'kuisioner'			=> $this->Admin_model->find_kuisioner($id_kuisioner),
				);
			}else{
				$data = array(
					'titile'			=> 'Panel Admin',
					'head_menu'			=> 'Master Kuisioner',
					'head_menu_form'	=> 'Edit Kuisioner',
					'kuisioner'			=> $this->Admin_model->find_kuisioner($id_kuisioner),
				);
			}

			$this->load->view('page/header', $data);
			$this->load->view('sysadmin/kuisioner/update', $data);
			$this->load->view('page/footer');
		}else{
			if(set_value('tipe_pertanyaan') == 'radio'){
				$vt = set_value('value_tipe');
			}else{
				$vt = '';
			}
			$data_kuisioner = array (
				'pertanyaan'		=> set_value('pertanyaan'),
				'jenis_pertanyaan'	=> set_value('jenis_pertanyaan'),
				'tipe_pertanyaan'	=> set_value('tipe_pertanyaan'),
				'value_tipe'		=> $vt,
			);
			$this->session->set_flashdata('update','Anda berhasil meng-update data kuisioner');
			$this->Admin_model->update_kuisioner($id_kuisioner, $data_kuisioner);
			redirect('sa/mk');
		}	
	}
	
	// MASTER DELETE KUISIONER
	public function delete_master_kuisioner($id_kuisioner) {
		$where = array('id_kuisioner' => $id_kuisioner);
		$this->User_model->delete($where,'t_kuisioner');
		$this->session->set_flashdata('delete','Anda berhasil menghapus data kuisioner');
		redirect('sa/mk');
	}
	
	// HASIL KUISIONER
	public function hasil_kuisioner() {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Hasil Kuisioner',
				'hasil_kuisioner'	=> $this->Admin_model->get_all_hasil_kuisioner(),
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Hasil Kuisioner',
				'hasil_kuisioner'	=> $this->Admin_model->get_all_hasil_kuisioner(),
			);
		}
		
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/kuisioner/result', $data);
		$this->load->view('page/footer');
	}
	
	// DETAIL HASIL KUISIONER
	public function detail_hasil_kuisioner($id_hasil_kuisioner) {
		if($this->session->userdata('level') == 'sysadmin'){
			$data = array(
				'titile'			=> 'Panel Sysadmin',
				'head_menu'			=> 'Hasil Kuisioner',
				'hasil_kuisioner'	=> $this->Admin_model->get_all_detail_hasil_kuisioner($id_hasil_kuisioner),
			);
		}else{
			$data = array(
				'titile'			=> 'Panel Admin',
				'head_menu'			=> 'Hasil Kuisioner',
				'hasil_kuisioner'	=> $this->Admin_model->get_all_detail_hasil_kuisioner($id_hasil_kuisioner),
			);
		}
		
		$this->load->view('page/header', $data);
		$this->load->view('sysadmin/kuisioner/detail_result', $data);
		$this->load->view('page/footer');
	}
}