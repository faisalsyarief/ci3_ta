        <?php $page = $_SERVER['PHP_SELF']; $sec = "10"; ?>
        <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><?php echo $head_menu; ?></h1>
            <div align="right">
                <a class="btn btn-primary" href="<?php echo site_url(); ?>Sysadmin/view_chart/<?php echo $date_views; ?>"> <span>Show Chart</span> </a>
            </div>
          </div>
          
          <!-- VALUE DASHBOARD -->
          <div id="reportrange" class="selectbox "  style="border: 1px solid #ddd; border-radius: 4px; padding:5px; background: #fff; cursor: pointer; margin: 0  0 4px 0;overflow: hidden; white-space: nowrap;">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"> <?php echo $date_select; ?> </i> &nbsp;
              <input type="date" class="form-control" onchange="changeSession()" name="selectdaterangev" id="selectdaterangev"/>
              <input type="hidden" class="form-control" id="date_views" value="<?php echo $date_views; ?>"/>
          </div>
          <!-- Content Row -->
          <div class="row">
            <div class="col-xl-12 col-lg-7">
              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <?php $explode_data_views = explode("_", $date_views);
                        $count_data_views = count($explode_data_views); ?>
                  <h6 class="m-0 font-weight-bold text-primary">Chart Kuisioner
                    <?php $viewsxx=""; for($iv=0; $iv<$count_data_views; $iv++){
                        if(($count_data_views-1) == $iv){
                            $viewsxx .= $explode_data_views[$iv];
                        }else{
                            $viewsxx .= $explode_data_views[$iv]." ";
                        }
                        echo $viewsxx;
                    } ?>
                  </h6>
                </div>
                <div class="card-body">
                  <div class="chart-bar">
                    <canvas id="<?php echo $viewsxx; ?>"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->