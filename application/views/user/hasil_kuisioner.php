        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3"></div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <!-- <th>Pertanyaan</th>
                      <th>Jawaban</th> -->
                      <th>Tanggal Kuisioner</th>
                      <th>Detail</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No.</th>
                      <!-- <th>Pertanyaan</th>
                      <th>Jawaban</th> -->
                      <th>Tanggal Kuisioner</th>
                      <th>Detail</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    
                    <?php if($hasil_kuisioner == FALSE){ ?>
                        <tr>
                            <td colspan='7' align='center'>TIDAK ADA DATA</td>
                        </tr>
                    <?php }else{ ?>
                        <?php $no=1; foreach($hasil_kuisioner as $hasil_kuisioners): ?>
                            <tr>
                                <td><?= $no; ?></td>
                                <!-- <td><?= $hasil_kuisioners->pertanyaan; ?></td>
                                <td><?= $hasil_kuisioners->jawaban; ?></td> -->
                                <td><?= $hasil_kuisioners->tgl_pengisian; ?></td>
                                <td>
                                    <a class="btn btn-info btn-icon-split" href="<?php echo site_url(); ?>u/dhk/<?=$hasil_kuisioners->id_hasil_kuisioner?>">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-arrow-right"></i>
                                        </span>
                                    </a>
                                </td>
                                

                            </tr>
                        <?php $no++; endforeach; ?>
                    <?php } ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
                
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->