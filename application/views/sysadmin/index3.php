        <?php $page = $_SERVER['PHP_SELF']; $sec = "10"; ?>
        <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800"><?php echo $head_menu; ?></h1>
            
            <div align="right">
                <a class="btn btn-primary" href="<?php echo site_url(); ?>sa"> <span>All Chart</span> </a>
                <a class="btn btn-primary" href="<?php echo site_url(); ?>Sysadmin/index_page1" role="button">1</a>
                <a class="btn btn-primary" href="<?php echo site_url(); ?>Sysadmin/index_page2" role="button">2</a>
                <a class="btn btn-outline-primary" href="<?php echo site_url(); ?>Sysadmin/index_page3" role="button">3</a>
            </div>
          </div>
          
          <!-- VALUE DASHBOARD -->
          <!-- Content Row -->
          <div class="row">
            <?php $nox=1; foreach($data_picture as $data_pictures):
              if($nox==1 || $nox==2){
                ?>
            <div class="col-xl-6 col-lg-8">
              <!-- Bar Chart -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Proses Fallure Picture</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div class="col-sm-12" align="center">
                            <a href="<?php echo site_url(); ?>assets/images/<?= $data_pictures->file; ?>">
                                <img src="<?php echo site_url(); ?>assets/images/<?= $data_pictures->file; ?>" alt="Proses Fallure Picture" height="350px" width="350px"> 
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
              <?php } $nox++; endforeach; ?>
          </div>
          
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->