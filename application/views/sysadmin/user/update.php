<?php
	$id_user = $user->id_user;
if($this->input->post('is_submitted')){
	$nama_user  = set_value('nama_user');
	$username   = set_value('username');
	$password   = set_value('password');
	$email      = set_value('email');
	$jk         = set_value('jk');
	$level      = set_value('level ');
	$send_email = set_value('send_email ');
}else{
	$nama_user  = $user->nama_user;
	$username   = $user->username;
	$password   = $user->password;
	$email      = $user->email;
	$jk         = $user->jk;
	$level      = $user->level;
	$send_email = $user->send_email;
}
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
                
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $head_menu_form; ?></h6>
            </div>

                <div class="card-body">
                    <?=form_open_multipart('sa/uu/' . $id_user, ['class'=>'form-horizontal'])?> 
                        <div class="form-group">
                        <?php $error = form_error("nama_user", "<p class='text-danger'>", '</p>'); ?>
                            <label for="nama_user" class="col-sm-2 control-label">Nama User (*)</label>
                            <div class="col-sm-12">
                                <input autocomplete="off" type="text" class="form-control" value="<?= $nama_user ?>" id="nama_user" name="nama_user" placeholder="Nama User" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("jk", "<p class='text-danger'>", '</p>'); ?>
                            <label for="jk" class="col-sm-2 control-label">Jenis Kelamin (*)</label>
                            <div class="col-sm-12">
                                <input type="radio" id="male" name="jk" value="Laki-Laki" <?= $jk=='Laki-Laki'?'checked':''; ?>>
                                <label for="male">Laki-Laki</label>&nbsp&nbsp&nbsp
                                <input type="radio" id="female" name="jk" value="Perempuan" <?= $jk=='Perempuan'?'checked':''; ?>>
                                <label for="female">Perempuan</label>&nbsp&nbsp&nbsp
                                <input type="radio" id="other" name="jk" value="Lain-Lain" <?= $jk=='Lain-Lain'?'checked':''; ?>>
                                <label for="other">Lain-Lain</label> 
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("email", "<p class='text-danger'>", '</p>'); ?>
                            <label for="email" class="col-sm-2 control-label">E-Mail (*)</label>
                            <div class="col-sm-12">
                                <input autocomplete="off" type="email" class="form-control" value="<?= $email ?>" id="email" name="email" placeholder="E-Mail" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("username", "<p class='text-danger'>", '</p>'); ?>
                            <label for="username" class="col-sm-2 control-label">Username (*)</label>
                            <div class="col-sm-12">
                                <input autocomplete="off" type="username" class="form-control" value="<?= $username ?>" id="username" name="username" placeholder="Username" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("password", "<p class='text-danger'>", '</p>'); ?>
                            <label for="password" class="col-sm-2 control-label">Password (*)</label>
                            <div class="col-sm-12">
                                <input autocomplete="off" type="password" class="form-control" value="<?= $password ?>" id="password" name="password" placeholder="PASSWORD" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("level", "<p class='text-danger'>", '</p>'); ?>
                            <label for="level" class="col-sm-2 control-label">Level (*)</label>
                            <div class="col-sm-12">
                                <input type="radio" id="sysadmin" name="level" value="sysadmin" <?= $level=='sysadmin'?'checked':''; ?>>
                                <label for="sysadmin">Sysadmin</label> &nbsp&nbsp&nbsp
                                <input type="radio" id="admin" name="level" value="admin" <?= $level=='admin'?'checked':''; ?>>
                                <label for="admin">Admin</label> &nbsp&nbsp&nbsp
                                <input type="radio" id="user" name="level" value="user" <?= $level=='user'?'checked':''; ?>>
                                <label for="user">User</label> 
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("send_email", "<p class='text-danger'>", '</p>'); ?>
                            <label for="send_email" class="col-sm-4 control-label">Menerima Hasil Kuisioner dari E-mail? (*)</label>
                            <div class="col-sm-12">
                                <input type="radio" id="1" name="send_email" value="1" <?= $send_email=='1'?'checked':''; ?>>
                                <label for="1">Ya</label> &nbsp&nbsp&nbsp
                                <input type="radio" id="0" name="send_email" value="0" <?= $send_email=='0'?'checked':''; ?>>
                                <label for="0">Tidak</label>
                            </div>
                        <?php echo $error; ?>
                        </div>
                        
                        <div class="form-group" align="right">
                            <a href="<?php echo site_url(); ?>sa/mu" class="btn btn-primary">Kembali</a>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
                    
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->