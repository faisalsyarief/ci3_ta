        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800"><?php echo $head_menu; ?></h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
                
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $head_menu_form; ?></h6>
            </div>

                <div class="card-body">
                    <?=form_open_multipart('sa/ak',['class'=>'form-horizontal'])?>
                        <div class="form-group">
                        <?php $error = form_error("pertanyaan", "<p class='text-danger'>", '</p>'); ?>
                            <label for="pertanyaan" class="col-sm-2 control-label">Pertanyaan (*)</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control" id="pertanyaan" name="pertanyaan" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')"></textarea>
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("jenis_pertanyaan", "<p class='text-danger'>", '</p>'); ?>
                            <label for="jenis_pertanyaan" class="col-sm-2 control-label">Jenis Pertanyaan</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="jenis_pertanyaan" name="jenis_pertanyaan">
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group">
                        <?php $error = form_error("tipe_pertanyaan", "<p class='text-danger'>", '</p>'); ?>
                            <label for="tipe_pertanyaan" class="col-sm-2 control-label">Tipe Pertanyaan (*)</label>
                            <div class="col-sm-12">
                                <select id="tipe_pertanyaan" name="tipe_pertanyaan" onchange="tipe_pertanyaan_change(this.value)" class="form-control" required oninvalid="this.setCustomValidity('Field ini tidak boleh kosong')" oninput="setCustomValidity('')">
                                    <option value="">--- Pilih ---</option>
                                    <option value="text">Inputan / Text</option>
                                    <option value="number">Number / Nomor</option>
                                    <option value="radio">Radio Button</option>
                                    <option value="date">Tanggal</option>
                                </select>
                            </div>
                        <?php echo $error; ?>
                        </div>

                        <div class="form-group" style="display:none;" id="value_tipe">
                            <label for="value_tipe" class="col-sm-2 control-label">Value Tipe Pertanyaan</label>
                            <div class="col-sm-12">
                                <select name="value_tipe" class="form-control">
                                    <option value="">--- Pilih ---</option>
                                    <option value="Morning;Afternoon">Morning & Afternoon</option>
                                    <option value="Ya;Tidak">Ya & Tidak</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group" align="right">
                            <a href="<?php echo site_url(); ?>sa/mk" class="btn btn-primary">Kembali</a>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </form>
                </div>
                    
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      
    <script>
        function tipe_pertanyaan_change(value) {            
            document.getElementById('value_tipe').style.display = value == 'radio' ? '' : 'none';
        }
    </script>